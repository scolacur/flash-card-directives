app.controller('MainController', function ($scope, FlashCardsFactory, ScoreFactory) {
	// $scope.cheat = false;
	$scope.loading = false;

	$scope.getCards = function (category){
			$scope.loading = true;
			FlashCardsFactory.getFlashCards(category)
			.then(function(cardsArr) {
				ScoreFactory.correct = 0;
				ScoreFactory.incorrect = 0;
				$scope.flashCards = cardsArr;
				$scope.loading = false;
			}).catch(function(err){
				console.error(err, err.message);
			});
	};

	$scope.categories = [
	    'MongoDB',
	    'Express',
	    'Angular',
	    'Node'
	];

	$scope.getCategoryCards = function (category) {
		$scope.categories.active = category || "all";
		$scope.getCards(category);
	};

	// $scope.enableCheat = function(){
	// 	$scope.cheat = !$scope.cheat;
	// 	console.log('$scope.cheat on?', $scope.cheat);
	// };

	$scope.getCategoryCards();
});
