app.factory('FlashCardsFactory', function ($http) {

  function getFlashCards (category){
    var queryParams = {};
    if (category) queryParams.category = category;

    return $http.get('/cards', {params: queryParams})
    .then(function(response){
        return response.data;
    });
  }

  return { getFlashCards: getFlashCards };
});


    //$http.get doesn't return a promise for the flash cards, but a
    //promise for a server response object whose
    //.data IS the flashcards, which is why we need to
    //.then off of it yet
