app.directive('flashCard', function(ScoreFactory){
  return {
    restrict: 'E',
    scope: {
      card: '='
    },
    templateUrl: 'js/directives/flash-card/flash-card.html',
    link: function(scope, element, attributes){
        // scope.cheat = false;
      	scope.answerQuestion = function (answer, flashCard) {
      		if (!flashCard.answered) {
      			flashCard.answered = true;
      			flashCard.answeredCorrectly = answer.correct;
      			flashCard.answeredCorrectly ? ScoreFactory.correct++ : ScoreFactory.incorrect++;
      			console.log(ScoreFactory.correct);
      		}
      	};
        // scope.enableCheat = function(){
        //   scope.cheat = !scope.cheat;
        //   console.log('$scope.cheat on?', scope.cheat);
        // };
    }
  };
});
