app.directive('borderOnHover', function(){
  console.log('Hovering!');
  return {
    restrict: 'C',
    link: function(scope, element, attrs) {
        var border;
      element.on('mouseenter', function(){
        border = element.css('border');
        element.css('border', '2px solid black');
      });
      element.on('mouseleave', function(){
        // element.addClass('hover-border');
        element.css('border', border);
      });
    }
  };
});
